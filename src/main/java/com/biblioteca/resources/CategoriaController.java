package com.biblioteca.resources;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.biblioteca.dtos.CategoriaDto;
import com.biblioteca.dtos.LivroDto;
import com.biblioteca.models.Categoria;
import com.biblioteca.services.CategoriaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value = "/categorias")
@Api(value = "categorias")
public class CategoriaController {
	@Autowired
	private CategoriaService service;
	
	//buscar Categorias 
	@ApiOperation(value = "buscar-categorias")
	@RequestMapping(method =  RequestMethod.GET)
	public ResponseEntity<List<CategoriaDto>> getAll() {
		
		
		return ResponseEntity.ok(service.findAll());
		
		
		
	}
	
	
	@ApiOperation(value = "salvar-categorias")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<CategoriaDto>salvar(@RequestBody CategoriaDto dto){
		
		 return ResponseEntity.ok(service.salvar(dto));
	}
	
	
	
	
	//buscar cat id
	  @RequestMapping(value = "/{id}",method = RequestMethod.GET)
	ResponseEntity<CategoriaDto> findId(@PathVariable("id") Long id){
		  
		  
		  
		return ResponseEntity.ok(service.getCat(id));
		
	}
	
	

}
