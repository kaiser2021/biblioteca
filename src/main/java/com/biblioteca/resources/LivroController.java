package com.biblioteca.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.biblioteca.dtos.LivroDto;
import com.biblioteca.services.LivroService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value = "/livros")
@Api(value = "Livros")
public class LivroController {
	@Autowired
	private LivroService livroService;
	
	// buscar livros pelo Id
	
	
	@RequestMapping(value = "/{id}",method = RequestMethod.GET)
	public ResponseEntity<LivroDto>getByLivro(@PathVariable("id") Long id){
		
		
		return ResponseEntity.ok(livroService.getById(id));
		
	}
	
	
	
	
	
	// salvar livros
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Salvar Livros")
	public ResponseEntity<LivroDto> salvar(@RequestBody LivroDto dto) {
		
		
		
		return ResponseEntity.ok(livroService.salvar(dto));
		
		
		
		
	}
	
	
	
	
	// atualizar livros
	
	
	
		@RequestMapping(value = "/{id}",method = RequestMethod.PUT)
		ResponseEntity<LivroDto>updateLivro(@PathVariable("id") Long id, LivroDto dto) throws Exception{
			
			
			
			return ResponseEntity.ok(livroService.updateLivro(id, dto));
			
		}
}
