package com.biblioteca.dtos;




import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class LivroDto {
	
	
	//private Long id;
	private String titulo;
	private String nome;
	
	private Long categoriaId;

	

}
