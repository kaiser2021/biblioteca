package com.biblioteca.dtos;




import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class CategoriaDto {
	private Long id;
	private String nome;
	private String descricao;
	@Default
	
	private List<LivroDto>livros=new ArrayList<>();
}
