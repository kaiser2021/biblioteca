package com.biblioteca.services;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.biblioteca.dtos.CategoriaDto;
import com.biblioteca.models.Categoria;
import com.biblioteca.repositories.CategoriaRepository;

import modelMapperUtils.ModelMapperUtil;
import modelMapperUtils.ModelMapperUtils;

@Service
public class CategoriaService {
	@Autowired
	private CategoriaRepository categoriaRepository;
	@Autowired
	private ModelMapper modelMapper;
	 
	private ModelMapperUtils mapperUtils;
	//buscar categorias
	
	@SuppressWarnings("static-access")
	public List<CategoriaDto > findAll() {
		
		List<Categoria> categoria=categoriaRepository.findAll();
		
		
		return  mapperUtils.mapAll(categoria, CategoriaDto.class);
		
		
		
		
		
	}
	
	
	
	//buscar categoria id
	
	public CategoriaDto getCat(Long id) {
		
	Categoria cat=categoriaRepository.findById(id).orElse(null);
		return modelMapper.map(cat, CategoriaDto.class);
		
		
	}
	
	
	
	
	
	// cadastrar uma categoria
	
	public CategoriaDto salvar(CategoriaDto dto) {
		
		
		 Categoria categoria=modelMapper.map(dto, Categoria.class);
		 
		 categoria=categoriaRepository.save(categoria);
		 
		
		
		return dto;
		
	}
	
	
	
	
	

}
