package com.biblioteca.services;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.biblioteca.dtos.LivroDto;
import com.biblioteca.models.Categoria;
import com.biblioteca.models.Livro;
import com.biblioteca.repositories.CategoriaRepository;
import com.biblioteca.repositories.LivroRepository;

import exceptionsHandler.ObjetoNotFound;

@Service
public class LivroService {

	@Autowired
	private LivroRepository livroRepository;
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private CategoriaRepository categoriaRepository;

	// buscar livros

	public LivroDto getById(Long id) {

		Livro livro = livroRepository.findById(id).orElse(null);
		

		return modelMapper.map(livro, LivroDto.class);

	}

//salvar livros

	public LivroDto salvar(LivroDto dto) {

		Livro livro = modelMapper.map(dto, Livro.class);
		// livro.setCategoria(categoriaRepository.findById(dto.getCategoriaId()).orElse(null));

		livro = livroRepository.save(livro);

		return dto;
	}

	// atualizar livros

	public LivroDto updateLivro(Long id, LivroDto dto) throws Exception {

		Livro livro = modelMapper.map(dto, Livro.class);
		livro.setId(id);
		
		if (categoriaRepository.findById(livro.getCategoria().getId()).orElse(null) != null) {
			livro = livroRepository.save(livro);
		}else {
			
			
		throw new ObjetoNotFound("categoria do Livro não encontrado");
		}
		

		return dto;

	}

}
