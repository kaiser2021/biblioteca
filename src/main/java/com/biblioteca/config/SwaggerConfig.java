package com.biblioteca.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2

public class SwaggerConfig {
	
	

	 @Bean
	    public Docket api() {
	        return new Docket(DocumentationType.SWAGGER_2)
	          .select()
	          .apis(RequestHandlerSelectors.any())
	          .paths(PathSelectors.any())
	          .build()
	          .apiInfo(metaInfo());
	          
	        
	        
	    }

	 
	 
	 private ApiInfo metaInfo() {

	        @SuppressWarnings("deprecation")
			ApiInfo apiInfo=new ApiInfo("Api Bibliotecas","Ivan Nilson Kaiser","Versão 1.0.0",
	        		"gringo9994@hotmail.com","Versão de Teste","ivan@teste","Api@copy")
	                
	        ;

	        return apiInfo;
	
	

	 }

	   


}
